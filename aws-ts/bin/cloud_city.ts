#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { CloudCityStack } from '../lib/cloud_city-stack';
import { CloudCityBuildStack } from '../lib/cloud_city-build-stack';

const app = new cdk.App();

const cloudCityStack = new CloudCityStack(app, 'CloudCity', {
  env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },
  description: 'Process OSM2City for the entire planet and build Cloud City',
});

const cloudCityBuildStack = new CloudCityBuildStack(app, 'CloudCityBuilder', {
  env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },
  description: 'Process OSM2City for the entire planet and build Cloud City',
  table: cloudCityStack.table,
});

cdk.Tags.of(app).add('app', 'cloud-city');

app.synth();
