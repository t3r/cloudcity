import * as cdk from 'aws-cdk-lib';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as efs from 'aws-cdk-lib/aws-efs';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';
import * as batch from 'aws-cdk-lib/aws-batch';
import { Construct } from 'constructs';

import { CloudCityVpc } from './constructs/cloud-city-vpc';
import { CloudCityFileSystem } from './constructs/cloud-city-filesystem';
import { CloudCityBatch } from './constructs/cloud-city-batch';
import { CloudCitySteps } from './constructs/cloud-city-steps';

export interface CloudCityBuildStackProps extends cdk.StackProps {
   table: dynamodb.TableV2;
}

export class CloudCityBuildStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props: CloudCityBuildStackProps ) {
    super(scope, id, props);

    const { vpc } = new CloudCityVpc(this, 'VPC');
    const { accessPoint, fileSystem } = new CloudCityFileSystem( this, id + '-EFS', { vpc } );
    const { fargateQueue, ec2Queue } = new CloudCityBatch( this, id + 'Batch', {
      vpc,
      fileSystem,
      accessPoint,
      table: props.table
    });

    const { stateMachine } = new CloudCitySteps( this, id + 'Sfn', {
      fargateQueue: fargateQueue,
      ec2Queue: ec2Queue,
      table: props.table,
    });
  }

}
