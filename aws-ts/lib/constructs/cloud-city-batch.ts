import * as cdk from 'aws-cdk-lib';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as batch from 'aws-cdk-lib/aws-batch';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as ecs from 'aws-cdk-lib/aws-ecs';
import { Construct } from 'constructs';

export class CloudCityBatch extends Construct {
  public readonly ec2Queue: batch.JobQueue;
  public readonly spotQueue: batch.JobQueue;
  public readonly fargateQueue: batch.JobQueue;
  public readonly taskRole: iam.Role;

  constructor(scope: Construct, id: string, props: any ) {
    super(scope, id);

    const builderImageName = new cdk.CfnParameter(this, 'builderImage', {
      description: 'Image for builder',
//      default: 'torstend/cloudcity-builder',
      default: 'registry.gitlab.com/t3r/cloudcity/builder:61075fbd',
      type: 'String',
      allowedPattern: '^.*$', // allowed pattern for the parameter
      minLength: 3, // minimum length of the parameter
      maxLength: 200, // maximum length of the parameter
    }).valueAsString // get the value of the parameter as string

    const taskRole =  new iam.Role(this, id + '-TaskRole', {
      assumedBy: new iam.ServicePrincipal('ecs-tasks.amazonaws.com'),
      description: 'Task Role for cloud-city',
    });

    taskRole.attachInlinePolicy(new iam.Policy(this, id + '-AllowEfs', {
      statements: [
        new iam.PolicyStatement({
          actions: [ "elasticfilesystem:ClientMount", "elasticfilesystem:ClientWrite" ],
          effect: iam.Effect.ALLOW,
          resources: [ props.fileSystem.fileSystemArn ]
        })
      ]
    }));

    taskRole.attachInlinePolicy(new iam.Policy(this, id + '-AllowSecrets', {
      statements: [
        new iam.PolicyStatement({
          actions: [ "kms:Decrypt", "secretsmanager:GetSecretValue" ],
          effect: iam.Effect.ALLOW,
          resources: [ "arn:aws:secretsmanager:eu-central-1:533267260386:secret:gitlab-docker-cIw1N7",
                       "arn:aws:kms:eu-central-1:533267260386:aws/secretsmanager", ]
        })
      ]
    }));

    this.ec2Queue = new batch.JobQueue(scope, id + 'OnDemQ', {
      computeEnvironments: [{
        computeEnvironment: new batch.ManagedEc2EcsComputeEnvironment(scope, id + 'OnDemEnv', {
          vpc: props.vpc,
          vpcSubnets: { subnetType: ec2.SubnetType.PUBLIC },
          allocationStrategy: batch.AllocationStrategy.BEST_FIT_PROGRESSIVE,
          // instanceClasses: [], // no preference, let AWS choose
          // securityGroups: [ ], // happy with default sg
          enabled: true,
          //instanceRole: null, // leave it unset, we'll get a nice one
          // serviceRole: null, // not required, leave as default
          minvCpus: 0,
          maxvCpus: 128,
        }),
        order: 0,
      }],
    });

    this.spotQueue = new batch.JobQueue(scope, id + 'SpotQ', {
      computeEnvironments: [{
        computeEnvironment: new batch.ManagedEc2EcsComputeEnvironment(scope, id + 'SpotEnv', {
          vpc: props.vpc,
          vpcSubnets: { subnetType: ec2.SubnetType.PUBLIC },
          allocationStrategy: batch.AllocationStrategy.BEST_FIT_PROGRESSIVE,
          // instanceClasses: [], // no preference, let AWS choose
          // securityGroups: [ ], // happy with default sg
          spot: true,
          spotBidPercentage: 75,
          enabled: true,
          //instanceRole: null, // leave it unset, we'll get a nice one
          // serviceRole: null, // not required, leave as default
          minvCpus: 0,
          maxvCpus: 128,
        }),
        order: 0,
      }],
    });

    this.fargateQueue = new batch.JobQueue( this, 'FargateQ', {
      computeEnvironments: [{
        computeEnvironment: new batch.FargateComputeEnvironment(this, 'FargateEnv', {
          vpc: props.vpc,
          vpcSubnets: { subnetType: ec2.SubnetType.PUBLIC },
          enabled: true,
          maxvCpus: 32,
          replaceComputeEnvironment: true,
        }),
        order: 0,
      }],
    });

    const efsVolume = new batch.EfsVolume({
      containerPath: '/workspace',
      name: 'EFS',
      fileSystem: props.fileSystem,
      readonly: false, // Set to true if you want the volume to be read-only
      useJobRole: true,
      accessPointId: props.accessPoint.accessPointId,
      enableTransitEncryption: true,
      rootDirectory: '/',
    });

    const ContainerDefs = {
      osm2cityBuilder: {
        cpu: 1.0,
        memory: cdk.Size.gibibytes(6), // works for Berlin
        image: ecs.ContainerImage.fromRegistry(builderImageName),
        jobRole: taskRole,
        assignPublicIp: true, // why is this needed?
        volumes: [ efsVolume ],
        secrets: {
        },
        environment: {
          FG_ROOT: '/workspace/fg_root',
          FG_SCENERY: '/workspace/scenery',
          OSM2CITY_PATH_TO_OUTPUT: '/workspace/o2c-scenery',
          OSM2CITY_PATH_TO_PACKED: '/workspace/o2c-packed',
          O2C_PROCESSES: '1',
          OVERPASS_URI: 'https://overpass.kumi.systems/api/interpreter',
        },
        command: [
          'scripts/build.sh',
          '--tile', 'Ref::tile'
        ],
      },

    }

    const JobDefs = {

/*
      TerraSync: {
        propagateTags: true,
        container: new batch.EcsFargateContainerDefinition(this, 'terrasync', {
          cpu: 0.25,
          memory: cdk.Size.gibibytes(0.5),
          image: ecs.ContainerImage.fromRegistry('torstend/terrasync'),
          jobRole: taskRole,
          assignPublicIp: true,
          volumes: [ efsVolume ],
          command: [
            "-t","/workspace/scenery",
            "-u","Ref::url",
            "--only-subdir","Models" ],
        }),
        parameters :{
          url: "https://ukmirror.flightgear.org/fgscenery",
        }
      },
*/

      Ec2Builder: {
        propagateTags: true,
        container: new batch.EcsEc2ContainerDefinition(this, 'osm2city-ec2', ContainerDefs.osm2cityBuilder ),
        timeout: cdk.Duration.hours(2),
        parameters : {
          tile: '0'
        }
      },

      FgBuilder: {
        propagateTags: true,
        container: new batch.EcsFargateContainerDefinition(this, 'osm2city-fg', ContainerDefs.osm2cityBuilder ),
        timeout: cdk.Duration.hours(2),
        parameters : {
          tile: '0'
        }
      },

    };

    for( const [ name, jobDef ] of Object.entries(JobDefs) ) {
      const taskDefinition = new batch.EcsJobDefinition(this, name, jobDef );
//      JobDefs[t][TASKDEF] = taskDefinition;
    }


  }
}

