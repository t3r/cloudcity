import * as cdk from 'aws-cdk-lib';
import * as sfn from 'aws-cdk-lib/aws-stepfunctions';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as tasks from 'aws-cdk-lib/aws-stepfunctions-tasks';
import * as batch from 'aws-cdk-lib/aws-batch';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';


import { Construct } from 'constructs';

export interface CloudCityStepsProps extends cdk.StackProps {
  fargateQueue: batch.IJobQueue,
  ec2Queue: batch.IJobQueue,
  table: dynamodb.ITableV2,
}

export class CloudCitySteps extends Construct {
  public readonly stateMachine: sfn.IStateMachine;

  constructor(scope: Construct, id: string, props: CloudCityStepsProps ) {
    super(scope, id);

    const role =  new iam.Role(this, id + 'Role', {
      assumedBy: new iam.ServicePrincipal('states.amazonaws.com'),
      description: 'StateMachine Role for cloud-city',
    });

    role.attachInlinePolicy(new iam.Policy(this, id + 'StepFunction', {
/*
  https://ataiva.com/how-to-fix-is-not-authorized-to-create-managed-rule-in-aws-step-functions/
  add CloudWatchEventsFullAccess
*/
      statements: [
        new iam.PolicyStatement({
          actions: [  
            "batch:SubmitJob", 
            "batch:DescribeJobs", 
            "batch:TerminateJob", 
            "states:StartExecution" 
          ],
          effect: iam.Effect.ALLOW,
          resources: [ "*" ] 
        }),
        new iam.PolicyStatement({
          actions: [ 
            "xray:PutTraceSegments", 
            "xray:PutTelemetryRecords", 
            "xray:GetSamplingRules", 
            "xray:GetSamplingTargets" ],
          effect: iam.Effect.ALLOW,
          resources: [ "*" ] 
        })
      ]
    }));

    
    

    const pass = new DynamoQuery( this, id + 'Query', {
      table: props.table,
      resultPath: '$.queries',
    })

    this.stateMachine = new sfn.StateMachine( this, id + 'TileBuilder', {
      role,
      definitionBody: sfn.DefinitionBody.fromChainable( pass ),
    });  
  }
}


export interface DynamoQueryProps extends sfn.TaskStateBaseProps {
  readonly table: dynamodb.ITable;
  readonly expressionAttributeNames?: { [key: string]: string };

}

// Or this? https://docs.aws.amazon.com/cdk/v2/guide/cfn_layer.html

// https://github.com/kreuzwerker/aws-cdk/blob/a2e8dc5e3cff91e64e5062a8fed630fb46982dbf/packages/aws-cdk-lib/aws-stepfunctions-tasks/lib/dynamodb/get-item.ts
export class DynamoQuery extends sfn.TaskStateBase {
  protected readonly taskMetrics?: sfn.TaskMetricsConfig;
  protected readonly taskPolicies?: iam.PolicyStatement[];

  constructor(scope: Construct, id: string, private readonly props: DynamoQueryProps) {
    super(scope, id, props);

    this.taskPolicies = [
      new iam.PolicyStatement({
        resources: [
          cdk.Stack.of(this).formatArn({
            service: 'dynamodb',
            resource: 'table',
            resourceName: props.table.tableName,
          }),
        ],
        actions: [`dynamodb:Query`],
      }),
    ];
  }

  /**
   * @internal
   */
  protected _renderTask(): any {
    return {
      Resource: "arn:aws:states:::aws-sdk:dynamodb:query",
      Parameters: sfn.FieldUtils.renderObject({
        TableName: this.props.table.tableName,
        ExpressionAttributeNames: this.props.expressionAttributeNames,
      }),
    };
  }

}

