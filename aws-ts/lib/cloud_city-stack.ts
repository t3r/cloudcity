import * as path from 'path';
import * as cdk from 'aws-cdk-lib';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as cognito from 'aws-cdk-lib/aws-cognito';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as s3_deployment from 'aws-cdk-lib/aws-s3-deployment';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as cloudfront from 'aws-cdk-lib/aws-cloudfront';
import * as cloudfront_origins from 'aws-cdk-lib/aws-cloudfront-origins';
import * as apigateway from 'aws-cdk-lib/aws-apigateway';
import * as apigatewayv2 from 'aws-cdk-lib/aws-apigatewayv2';
import * as acm from 'aws-cdk-lib/aws-certificatemanager';
import { WebSocketLambdaIntegration } from 'aws-cdk-lib/aws-apigatewayv2-integrations';

import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';
import * as eventsources from 'aws-cdk-lib/aws-lambda-event-sources';

import { Construct } from 'constructs';

class CloudCityTilesTable extends Construct {
  public readonly table: dynamodb.TableV2;

  constructor(scope: Construct, id: string ) {
    super(scope, id);

    // DDB table to store tile states
    this.table = new dynamodb.TableV2(this, id, {
      billing: dynamodb.Billing.onDemand(),
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      partitionKey: { name: 'tile', type: dynamodb.AttributeType.NUMBER },
      sortKey: { name: 'timestamp', type: dynamodb.AttributeType.NUMBER },
      dynamoStream: dynamodb.StreamViewType.NEW_IMAGE, // Enable DynamoDB Stream
    });
    this.table.addGlobalSecondaryIndex({
      indexName: 'gsi_10x10',
      partitionKey: { name: 'ten_ten', type: dynamodb.AttributeType.STRING },
    });
    this.table.addGlobalSecondaryIndex({
      indexName: 'gsi_1x1',
      partitionKey: { name: 'one_one', type: dynamodb.AttributeType.STRING },
    });
    this.table.addGlobalSecondaryIndex({
      indexName: 'gsi_status',
      partitionKey: { name: 'status', type: dynamodb.AttributeType.STRING },
    });

  }
}

class CloudCitySiteBucket extends Construct {
  public readonly bucket: s3.Bucket;

  constructor(scope: Construct, id: string ) {
    super(scope, id);

    this.bucket = new s3.Bucket(this, id, {
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      encryption: s3.BucketEncryption.S3_MANAGED,
      enforceSSL: true,
      versioned: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    new s3_deployment.BucketDeployment(this, id + '-SiteDeployment', {
      sources: [s3_deployment.Source.asset(path.join(__dirname, 'www'))],
      destinationBucket: this.bucket
    });
  }
}

interface CloudCityApiProps {
  tileTable: dynamodb.ITableV2,
}

class CloudCityApi extends Construct {
  public readonly api: apigateway.RestApi;
  public readonly userPool: cognito.UserPool;
  public readonly userPoolClient: cognito.IUserPoolClient;
  public readonly userPoolDomain: cognito.IUserPoolDomain;
  public readonly websocketTable: dynamodb.ITableV2;
  public readonly websocketApi: apigatewayv2.WebSocketApi;
  public readonly websocketIntegrationLambda: lambda.IFunction;
  public readonly webSocketStage: apigatewayv2.WebSocketStage;
  public readonly streamTriggerLambda: lambda.IFunction;
  public readonly apiEndpoint: string;


  constructor(scope: cdk.Stack, id: string, props: CloudCityApiProps ) {
    super(scope, id);

    this.userPool = new cognito.UserPool( this, id + '-Pool', {
      accountRecovery: cognito.AccountRecovery.EMAIL_ONLY,
      signInAliases: { email: true },
      selfSignUpEnabled: true,
    });
    this.userPoolClient = this.userPool.addClient('Client', {
      authFlows: {
        userSrp: true,
      },
      oAuth: {
        flows: {
          implicitCodeGrant: true,
        },
        callbackUrls: [
          'http://localhost:8080/',
          'https://cloudcity.flightgear.org/',
        ],
      },
    });
    this.userPoolDomain = this.userPool.addDomain('CognitoDomain', {
      cognitoDomain: {
        domainPrefix: 'cloudcity2',
      },
    });

    const authUrl = `https://${this.userPoolDomain.domainName}.auth.${scope.region}.amazoncognito.com/oauth2/authorize?client_id=${this.userPoolClient.userPoolClientId}&response_type=token&scope=aws.cognito.signin.user.admin+email+openid+phone+profile&redirect_uri=https%3A%2F%2Fcloudcity.flightgear.org%2F`

    const userPoolAdminGroup = new cognito.CfnUserPoolGroup(this, id + '-AdminGroup', {
      userPoolId: this.userPool.userPoolId,
      description: 'Admins of CloudCity, can do admin stuff.',
      groupName: 'admins',
      precedence: 0,
    });

    const userPoolSupportersGroup = new cognito.CfnUserPoolGroup(this, id + '-SupportersGroup', {
      userPoolId: this.userPool.userPoolId,
      description: 'Supporters of CloudCity, can see more than others.',
      groupName: 'supporters',
      precedence: 10,
    });

    // The websocket API
    this.websocketApi =  new apigatewayv2.WebSocketApi(this, id + '-WS', {
      description: 'CloudCity websocket API',
    });

    this.webSocketStage = new apigatewayv2.WebSocketStage(this, id + 'stage-dev', {
      webSocketApi: this.websocketApi,
      stageName: 'dev',
      autoDeploy: true,
    });
    this.apiEndpoint = `https://${this.websocketApi.apiId}.execute-api.${scope.region}.amazonaws.com/${this.webSocketStage.stageName}`;

    // DDB table to store websocket connection ids
    this.websocketTable = new dynamodb.TableV2(this, 'WSConnections', {
      billing: dynamodb.Billing.onDemand(),
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      partitionKey: { name: 'ConnectionId', type: dynamodb.AttributeType.STRING }
    });
  
    // the websocket integration lambda
    this.websocketIntegrationLambda = new lambda.Function(this, 'WSIntegration', {
      runtime: lambda.Runtime.NODEJS_20_X,
      handler: 'index.handler',
      code: lambda.Code.fromAsset(path.join(__dirname, 'lambdas', 'wsconnect' )),
      description: 'CloudCity integration handler for websocket API',
      environment: {
        CONNECTIONS_TABLE: this.websocketTable.tableName,
        TILES_TABLE: props.tileTable.tableName,
      },
    });
    // needs to write connection ids to the websock table
    this.websocketTable.grantWriteData(this.websocketIntegrationLambda);
    props.tileTable.grantReadWriteData(this.websocketIntegrationLambda);

    // the trigger function for the tile table stream
    this.streamTriggerLambda = new lambda.Function(this, 'TileChangeTrigger', {
      runtime: lambda.Runtime.NODEJS_20_X,
      handler: 'index.handler',
      code: lambda.Code.fromAsset(path.join(__dirname, 'lambdas', 'trigger' )),
      description: 'Trigger function for the CloudCity dynamodb tile table stream',
      environment: {
        API_GATEWAY_ENDPOINT: this.apiEndpoint,
        CONNECTIONS_TABLE: this.websocketTable.tableName
      },
    });
    // needs to read the websocket connection ids
    this.websocketTable.grantReadData(this.streamTriggerLambda);

    // connect tileTable and streamTriggerLambda
    props.tileTable.grantStreamRead(this.streamTriggerLambda);
    this.streamTriggerLambda.addEventSource(
      new eventsources.DynamoEventSource(props.tileTable, {
        startingPosition: lambda.StartingPosition.LATEST,
        batchSize: 100,
        maxBatchingWindow: cdk.Duration.seconds(5),
        retryAttempts: 3,
      })
    );

    // allow manage connections of the websocket api (POST to @connection)
    this.websocketApi.grantManageConnections(this.streamTriggerLambda);

    this.websocketApi.addRoute('$connect', {
      integration: new WebSocketLambdaIntegration(id+'WSIntegrationConnect', this.websocketIntegrationLambda),
    });
    this.websocketApi.addRoute('$disconnect', {
      integration: new WebSocketLambdaIntegration('WSIntegrationDisconnect', this.websocketIntegrationLambda),
    });
    const defaultRoute = this.websocketApi.addRoute('$default', {
      integration: new WebSocketLambdaIntegration('WSIntegrationDefault', this.websocketIntegrationLambda),
      //returnResponse: true,
    });

    // Enable route-response for $default route
    const cfn_route_response = new apigatewayv2.CfnRouteResponse(this, "DefaultResponse", {
      apiId: this.websocketApi.apiId,
      routeId: defaultRoute.routeId,
      routeResponseKey: '$default'
    });
    
    // REST API
    const apiLambda =  new lambda.Function( this, 'ApiIntegrationLambda', {
      runtime: lambda.Runtime.NODEJS_20_X,
      initialPolicy: [
        new iam.PolicyStatement({
          effect: iam.Effect.ALLOW,
          actions: [ 'dynamodb:Query' ],
          resources: [props.tileTable.tableArn + '/*'],
        }),
      ],
      description: 'REST API Implementation for the CloudCity',
      code: lambda.Code.fromAsset(path.join(__dirname, 'lambdas', 'api' )),
      handler: 'index.handler',
      timeout: cdk.Duration.seconds(5),
      environment: {
        TABLE_NAME: props.tileTable.tableName,
      },
    });

    props.tileTable.grantReadWriteData( apiLambda );

    const apiIntegration = new apigateway.LambdaIntegration( apiLambda );
    
    this.api = new apigateway.RestApi( this, id + '-REST', {
      description: 'REST API for CloudCity',
      deployOptions: {
        stageName: 'api',
      },
      binaryMediaTypes: [],
      minCompressionSize: cdk.Size.mebibytes( 1 ),
      endpointConfiguration: {
        types: [ apigateway.EndpointType.REGIONAL ],
      },
    });

    const restApiAuthorizer = new apigateway.CognitoUserPoolsAuthorizer( this, 'Authorizer', {
      cognitoUserPools: [ this.userPool ],
    });

    this.api.root.addResource('tile').addResource('{id}')
      .addCorsPreflight({
          allowOrigins: apigateway.Cors.ALL_ORIGINS,
          allowMethods: [ 'GET', 'POST' ],
      })
      .resource.addMethod('GET', apiIntegration )
      .resource .addMethod('POST', apiIntegration, {
        authorizer: restApiAuthorizer,
        authorizationType: apigateway.AuthorizationType.COGNITO
      })

    this.api.root.addResource('1x1').addResource('{id}')
      .addCorsPreflight({
          allowOrigins: apigateway.Cors.ALL_ORIGINS,
          allowMethods: [ 'GET', 'POST' ],
      })
      .resource.addMethod('GET', apiIntegration )
      .resource.addMethod('POST', apiIntegration, {
        authorizer: restApiAuthorizer,
        authorizationType: apigateway.AuthorizationType.COGNITO
      })

    this.api.root.addResource('10x10').addResource('{id}')
      .addCorsPreflight({
          allowOrigins: apigateway.Cors.ALL_ORIGINS,
          allowMethods: [ 'GET', 'POST' ],
      })
      .resource.addMethod('GET', apiIntegration )
      .resource.addMethod('POST', apiIntegration, {
        authorizer: restApiAuthorizer,
        authorizationType: apigateway.AuthorizationType.COGNITO
      })

      const redirectIntegration = new apigateway.MockIntegration({
        integrationResponses: [
          {
            statusCode: '301',
            responseParameters: {
              'method.response.header.Location': `'${authUrl}'`,
            },
          },
        ],
        requestTemplates: {
          'application/json': JSON.stringify({
            statusCode: 301,
          }),
        },
        passthroughBehavior: apigateway.PassthroughBehavior.NEVER,
      });

      const redirectMethod = this.api.root.addResource('login').addMethod(
        'GET',
        redirectIntegration,
        {
          methodResponses: [
            {
              statusCode: '301',
              responseParameters: {
                'method.response.header.Location': true,
              },
            },
          ],
        }
      );
  }
};

interface CloudCityDistributionProps {
  bucket: s3.Bucket;
  api: apigateway.IRestApi;
  websocketApi: apigatewayv2.IWebSocketApi;
  userPool: cognito.UserPool;
  domainName?: string;
  domainNameCertificateArn: string;
}
class CloudCityDistribution extends Construct {
  public readonly distribution: cloudfront.Distribution;

  constructor(scope: cdk.Stack, id: string, props: CloudCityDistributionProps ) {
    super( scope, id );

    this.distribution = new cloudfront.Distribution( this, id, {
      domainNames: props.domainName && props.domainName.length ? [props.domainName] : [],
      certificate: props.domainName ? acm.Certificate.fromCertificateArn(scope, 'Cert', props.domainNameCertificateArn) : undefined,
      priceClass: cloudfront.PriceClass.PRICE_CLASS_100,
      defaultBehavior: {
        origin: new cloudfront_origins.S3Origin(props.bucket),
        viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
      },
      additionalBehaviors: {
        '/api/*': {
          allowedMethods: cloudfront.AllowedMethods.ALLOW_ALL,
          cachedMethods: cloudfront.CachedMethods.CACHE_GET_HEAD_OPTIONS,
          origin: new cloudfront_origins.HttpOrigin( `${props.api.restApiId}.execute-api.${scope.region}.${scope.urlSuffix}`),
          viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          cachePolicy: new cloudfront.CachePolicy(this, 'PublicApiCachePolicy', {

            minTtl: cdk.Duration.seconds(10), //FIXME: pick a usable value for prod
            defaultTtl: cdk.Duration.seconds(20), //FIXME: pick a usable value for prod
            maxTtl: cdk.Duration.seconds(30), //FIXME: pick a usable value for prod

            comment: 'Pass the query string and set TTL',
            enableAcceptEncodingBrotli: true,
            enableAcceptEncodingGzip: true,
            queryStringBehavior: cloudfront.CacheQueryStringBehavior.all(),
          }),
        },
        '/dev': {
          allowedMethods: cloudfront.AllowedMethods.ALLOW_ALL,
          compress: false,
          origin: new cloudfront_origins.HttpOrigin( `${props.websocketApi.apiId}.execute-api.${scope.region}.${scope.urlSuffix}`),
          viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          cachePolicy: cloudfront.CachePolicy.CACHING_DISABLED,
          originRequestPolicy: cloudfront.OriginRequestPolicy.ALL_VIEWER_EXCEPT_HOST_HEADER
        }
      },
      defaultRootObject: 'index.html',
    });

    const bucketPolicy = new iam.PolicyStatement({
      effect: iam.Effect.ALLOW,
      actions: ['s3:GetObject'],
      resources: [props.bucket.arnForObjects('*')],
      principals: [new iam.ServicePrincipal('cloudfront.amazonaws.com')],
    });
    bucketPolicy.addCondition( "StringEquals", {
      "aws:SourceArn": this.distribution.distributionId,
    });
    props.bucket.addToResourcePolicy( bucketPolicy );
  }
};

export class CloudCityStack extends cdk.Stack {
  public readonly table: dynamodb.TableV2;

  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const domainParameter = new cdk.CfnParameter(this, 'DomainName', {
      type: 'String',
      description: 'Alternate domain name to use for the site (requires a certificate)',
      default: 'cloudcity.flightgear.org'
    });

    const certParameter = new cdk.CfnParameter( this, 'DomainCertificate', {
      type: 'String',
      description: 'Domain certificate ARN',
      default: 'arn:aws:acm:us-east-1:533267260386:certificate/8e3e7731-f21e-4e72-b17a-6c137a95ad74',
    });

    const { table }        = new CloudCityTilesTable( this, id + '-Table' );
    this.table = table;
    const { bucket }       = new CloudCitySiteBucket( this, id + '-SiteBucket' );
    const { api,websocketApi, userPool } = new CloudCityApi( this, id + '-Api', { 
      tileTable: table
    });
    const { distribution } = new CloudCityDistribution( this, id + '-Distribution', {
      domainName: domainParameter.valueAsString,
      domainNameCertificateArn: certParameter.valueAsString,
      bucket,
      api,
      websocketApi,
      userPool
    });
    
    new cdk.CfnOutput(this, 'Distribution', {
      value: distribution.domainName
    });

    new cdk.CfnOutput(this, 'TilesTable', {
      value: table.tableName
    });
  }
}
