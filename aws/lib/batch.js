const { CfnParameter, Size, Duration } = require('aws-cdk-lib');
const ec2 = require('aws-cdk-lib/aws-ec2');
const iam = require('aws-cdk-lib/aws-iam');
const batch = require('aws-cdk-lib/aws-batch');
const secretsmanager = require('aws-cdk-lib/aws-secretsmanager');
const ecs = require('aws-cdk-lib/aws-ecs');
const { Construct } = require('constructs');

function createEc2Queue(scope,id,props) {
  return new batch.JobQueue(scope, id + 'Q', {
    computeEnvironments: [{
      computeEnvironment: new batch.ManagedEc2EcsComputeEnvironment(scope, id + 'Env', {
        vpc: props.vpc,
        vpcSubnets: { subnetType: ec2.SubnetType.PUBLIC },
        allocationStrategy: batch.AllocationStrategy.BEST_FIT_PROGRESSIVE,
        // instanceClasses: [], // no preference, let AWS choose
        // securityGroups: [ ], // happy with default sg
        spot: !!props.spot,
        enabled: true,
        //instanceRole: null, // leave it unset, we'll get a nice one
        // serviceRole: null, // not required, leave as default
        minvCpus: 0,
        maxvCpus: 32,
      }),
      order: 0,
    }],
  });
}

const FGQ = Symbol("FGQ");
const EC2ODQ = Symbol("EC2ODQ");
const EC2SPQ = Symbol("EC2SPQ");
const TASKDEF = Symbol("TaskDef");
const JOBDEFS = Symbol("JobDefs");

class BatchConstruct extends Construct {
  constructor(scope, id, props) {
    super(scope, id, props);

    const taskRole =  new iam.Role(this, id + 'TaskRole', {
      assumedBy: new iam.ServicePrincipal('ecs-tasks.amazonaws.com'),
      description: 'Task Role for cloud-city',
    });

    taskRole.attachInlinePolicy(new iam.Policy(this, id + 'AllowEfs', {
      statements: [
        new iam.PolicyStatement({
          actions: [ "elasticfilesystem:ClientMount", "elasticfilesystem:ClientWrite" ],
          effect: iam.Effect.ALLOW,
          resources: [ props.efs.fileSystem.fileSystemArn ]
        })
      ]
    }));

    this[FGQ] = new batch.JobQueue( this, 'FargateOnDemandQ', {
      computeEnvironments: [{
        computeEnvironment: new batch.FargateComputeEnvironment(this, 'FargateOnDemandEnv', {
          vpc: props.vpc,
          vpcSubnets: { subnetType: ec2.SubnetType.PUBLIC },
          spot: false,
          enabled: true,
          maxvCpus: 4, //TODO: change to something realistic
          replaceComputeEnvironment: true,
        }),
        order: 0,
      }],
    });

    this[EC2ODQ] = createEc2Queue( this, 'Ec2OnDemand', Object.assign({ spot: false }, props) );
    this[EC2SPQ] = createEc2Queue( this, 'Ec2Spot', Object.assign({ spot: true }, props) );

    const efsVolume = new batch.EfsVolume({
      containerPath: '/workspace',
      name: 'EFS',
      fileSystem: props.efs.fileSystem,
      readOnly: false, // Set to true if you want the volume to be read-only
      useJobRole: true,
      accessPointId: props.efs.accessPoint.accessPointId,
      enableTransitEncryption: true,
      rootDirectory: '/',
    });

    const dbSecret = secretsmanager.Secret.fromSecretArn( this, "DBSecret", props.db.cluster.secret.secretArn );

    const importerImageName = new CfnParameter(this, 'importerImage', {
      description: 'Image for importer',
      default: 'torstend/cloudcity-importer',
      type: 'String',
      allowedPattern: '^.*$', // allowed pattern for the parameter
      minLength: 3, // minimum length of the parameter
      maxLength: 200, // maximum length of the parameter
    }).valueAsString // get the value of the parameter as string

    const builderImageName = new CfnParameter(this, 'builderImage', {
      description: 'Image for builder',
      default: 'torstend/cloudcity-builder',
      type: 'String',
      allowedPattern: '^.*$', // allowed pattern for the parameter
      minLength: 3, // minimum length of the parameter
      maxLength: 200, // maximum length of the parameter
    }).valueAsString // get the value of the parameter as string

    const ContainerDefs = {
      osm2cityBuilder: {
        cpu: 1.0,
        memory: Size.gibibytes(4),
        image: ecs.ContainerImage.fromRegistry(builderImageName),
        jobRole: taskRole,
        assignPublicIp: true,
        volumes: [ efsVolume ],
        secrets: {
          PGHOST: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'host'),
          PGUSER: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'username'),
          PGPASSWORD: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'password'),
          PGDATABASE: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'dbname'),
          PGPORT: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'port'),
        },
        environment: {
          FG_ROOT: '/workspace/fg_root',
          FG_SCENERY: '/workspace/scenery',
          OSM2CITY_PATH_TO_OUTPUT: '/workspace/o2c-scenery',
          OSM2CITY_PATH_TO_PACKED: '/workspace/o2c-packed',
          O2C_PROCESSES: '2',
        },
        command: [
          'scripts/build.sh',
          '--json', 'Ref::json'
        ],
      },

      dbImporter: {
          cpu: 1.0,
          memory: Size.gibibytes(8),
          image: ecs.ContainerImage.fromRegistry(importerImageName),
          jobRole: taskRole,
          assignPublicIp: true,
          ephemeralStorageSize: Size.gibibytes(150),
          volumes: [ efsVolume ],
          secrets: {
            PGHOST: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'host'),
            PGUSER: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'username'),
            PGPASSWORD: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'password'),
            PGDATABASE: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'dbname'),
            PGPORT: ecs.Secret.fromSecretsManagerVersion(dbSecret,{}, 'port'),
          },
          command: [
            'scripts/import.sh',
            '--input', '/workspace/planet.osm.pbf',
            '--json', 'Ref::json'
          ],
        }
    }

    this[JOBDEFS]= {

      FetchPlanetS3: {
        propagateTags: true,
        container: new batch.EcsFargateContainerDefinition(this, 'aws-cli', {
          cpu: 0.25,
          memory: Size.gibibytes(0.5),
          image: ecs.ContainerImage.fromRegistry('amazon/aws-cli'),
          jobRole: taskRole,
          assignPublicIp: true,
          volumes: [ efsVolume ],
          // s3://osm-planet-eu-central-1/
          // command: [ 's3','cp','--no-sign-request','s3://osm-pds/2024/planet-240101.osm.pbf', '/workspace/planet.osm.pbf' ],
          command: [ 's3','cp','--no-sign-request','Ref::url', '/workspace/planet.osm.pbf' ],
        }),
        parameters :{
          url: 's3://osm-pds/2024/planet-240101.osm.pbf',
        }
      },

      FgFetchCurl: {
        propagateTags: true,
        container: new batch.EcsFargateContainerDefinition(this, 'curl', {
          cpu: 0.25,
          memory: Size.gibibytes(0.5),
          image: ecs.ContainerImage.fromRegistry('curlimages/curl'),
          jobRole: taskRole,
          assignPublicIp: true,
          volumes: [ efsVolume ],
          command: ["-vL","--output","/workspace/planet.osm.pbf","Ref::url"],
        }),
        parameters :{
          url: 'https://download.geofabrik.de/europe/germany/hamburg-latest.osm.pbf',
        }
      },
/*
      TerraSync: {
        propagateTags: true,
        container: new batch.EcsFargateContainerDefinition(this, 'terrasync', {
          cpu: 0.25,
          memory: Size.gibibytes(0.5),
          image: ecs.ContainerImage.fromRegistry('torstend/terrasync'),
          jobRole: taskRole,
          assignPublicIp: true,
          volumes: [ efsVolume ],
          command: [
            "-t","/workspace/scenery",
            "-u","Ref::url",
            "--only-subdir","Models" ],
        }),
        parameters :{
          url: "https://ukmirror.flightgear.org/fgscenery",
        }
      },
*/
      Ec2ImportPlanet: {
        propagateTags: true,
        container: new batch.EcsEc2ContainerDefinition(this, 'o2cImporter-ec2', ContainerDefs.dbImporter ),
        parameters :{
          json: ''
        }
      },

      FgImportPlanet: {
        propagateTags: true,
        container: new batch.EcsFargateContainerDefinition(this, 'o2cImporter-fg', ContainerDefs.dbImporter ),
        parameters :{
          json: ''
        }
      },

      Ec2Builder: {
        propagateTags: true,
        container: new batch.EcsEc2ContainerDefinition(this, 'osm2city-ec2', ContainerDefs.osm2cityBuilder ),
        timeout: Duration.hours(2),
        parameters : {
          json: ''
        }
      },

      FgBuilder: {
        propagateTags: true,
        container: new batch.EcsFargateContainerDefinition(this, 'osm2city-fg', ContainerDefs.osm2cityBuilder ),
        timeout: Duration.hours(2),
        parameters : {
          json: '',
        }
      },

    };
    for( let t in this[JOBDEFS] ) {
      const taskDefinition = new batch.EcsJobDefinition(this, t, this[JOBDEFS][t] );
      this[JOBDEFS][t][TASKDEF] = taskDefinition;
    }

  }

  get ec2SpotQueue() {
    return this[EC2SPQ];
  }

  get ec2OnDemandQueue() {
    return this[EC2ODQ];
  }

  get fargateQueue() {
    return this[FGQ];
  }

  getTaskDefinition( jobDef ) {
    if( jobDef in this[JOBDEFS] )
      return this[JOBDEFS][jobDef][TASKDEF];
  }
}

module.exports = { BatchConstruct }
