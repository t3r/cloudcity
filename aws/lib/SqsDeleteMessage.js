const sfn = require('aws-cdk-lib/aws-stepfunctions');
const iam = require('aws-cdk-lib/aws-iam');

function integrationResourceArn(service, api ) {
  if (!service || !api) {
    throw new Error("Both 'service' and 'api' must be provided to build the resource ARN.");
  }
  return `arn:aws:states:::${service}:${api}`;
}

class SqsDeleteMessage extends sfn.TaskStateBase {

  constructor(scope, id, props) {
    super(scope, id, props);

    this.props = props;
    this.integrationPattern = props.integrationPattern ?? sfn.IntegrationPattern.REQUEST_RESPONSE;

    this.taskPolicies = [
      new iam.PolicyStatement({
        actions: ['sqs:DeleteMessage'],
        resources: [this.props.queue.queueArn],
      }),
    ];

    if (this.props.queue.encryptionMasterKey) {
      this.taskPolicies.push(
        new iam.PolicyStatement({
          actions: ['kms:Decrypt', 'kms:GenerateDataKey*'],
          resources: [this.props.queue.encryptionMasterKey.keyArn],
        }));
    }
  }

  _renderTask() {
     return {
      Resource: integrationResourceArn('aws-sdk:sqs', 'deleteMessage', this.integrationPattern),
      Parameters: sfn.FieldUtils.renderObject({
        QueueUrl: this.props.queue.queueUrl,
        'ReceiptHandle.$': this.props.receiptHandle,
      }),
    };
  }
}

module.exports = SqsDeleteMessage;
