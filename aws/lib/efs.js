const { RemovalPolicy } = require('aws-cdk-lib');
const efs = require('aws-cdk-lib/aws-efs');
const iam = require('aws-cdk-lib/aws-iam');
const ec2 = require('aws-cdk-lib/aws-ec2');
const { Construct } = require('constructs');

const FS = Symbol("FS");
const AP = Symbol("AP");
const SG = Symbol("SG");

class EfsConstruct extends Construct {

  constructor(scope, id, props) {
    super(scope, id, props);

    const securityGroup = new ec2.SecurityGroup( this, id + 'SecurityGroup', {
      vpc: props.vpc,
      description: 'Allow inbound NFS Traffic',
    });
    securityGroup.addIngressRule( ec2.Peer.anyIpv4(), ec2.Port.tcp(2049), 'allow nfs inbound' );

/*
 orig:
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "elasticfilesystem:ClientRootAccess",
                "elasticfilesystem:ClientWrite",
                "elasticfilesystem:ClientMount"
            ],
            "Resource": "arn:aws:elasticfilesystem:eu-central-1:533267260386:file-system/fs-022555c539e8a1d93",
            "Condition": {
                "Bool": {
                    "elasticfilesystem:AccessedViaMountTarget": "true"
                }
            }
        }
    ]
}
*/
/*
    const fileSystemPolicy = new iam.PolicyDocument({
      statements: [new iam.PolicyStatement({
        actions: [
          'elasticfilesystem:ClientWrite',
          'elasticfilesystem:ClientMount',
          'elasticfilesystem:ClientRootAccess',
        ],
        principals: [new iam.AccountRootPrincipal()],
        resources: ['*'], //FIXME: set efs-arn here
        conditions: {
          Bool: {
            'elasticfilesystem:AccessedViaMountTarget': 'true',
          },
        },
      })],
    });
*/

    this[FS] = new efs.FileSystem(this, id + 'FileSystem', {
      vpc: props.vpc,
      lifecyclePolicy: efs.LifecyclePolicy.AFTER_7_DAYS,
      transitionToArchivePolicy: efs.LifecyclePolicy.AFTER_30_DAYS,
      enableAutomaticBackups: false,
      performanceMode: efs.PerformanceMode.GENERAL_PURPOSE,
      throughputMode: efs.ThroughputMode.ELASTIC,
      outOfInfrequentAccessPolicy: efs.OutOfInfrequentAccessPolicy.AFTER_1_ACCESS,
      removalPolicy: RemovalPolicy.DESTROY,
      securityGroup,
 //     fileSystemPolicy,
    });

    this[AP] = new efs.AccessPoint( this, id + 'AccessPoint', {
      fileSystem: this.fileSystem,
       createAcl: {
         ownerGid: '1000',
         ownerUid: '1000',
         permissions: '0755',
       },
       path: '/cloud-city',
       posixUser: {
         gid: '1000',
         uid: '1000',
         // secondaryGids: ['secondaryGids'], // optional
       },
    });
  }

  get accessPoint() {
    return this[AP];
  }

  get fileSystem() {
    return this[FS];
  }
}


module.exports = { EfsConstruct }
