const { RemovalPolicy, Stack, } = require('aws-cdk-lib');
const s3 = require('aws-cdk-lib/aws-s3');
const ec2 = require('aws-cdk-lib/aws-ec2');
const ecr = require('aws-cdk-lib/aws-ecr');

const { EfsConstruct } = require('./efs.js');
const { DbConstruct } = require('./db.js');
const { BatchConstruct } = require('./batch.js');
const { CloudStateMachine } = require('./step.js');

class CloudCityStack extends Stack {
  constructor(scope, id, props) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, 'VPC', {
      natGateways: 0,
      maxAzs: 3,
      subnetConfiguration: [
      {
        name: 'public-cloudcity-1',
        subnetType: ec2.SubnetType.PUBLIC,
      },
      {
        name: "private-subnet",
        subnetType: ec2.SubnetType.ISOLATED,
        cidrMask: 24,
      },
      ],
    });

//    const vpc = ec2.Vpc.fromLookup(this, "VPC", {
//      isDefault: true,
//    });

    const terrasync = new s3.Bucket(this, 'TerraSync', {
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      encryption: s3.BucketEncryption.S3_MANAGED,
      enforceSSL: true,
      versioned: false,
      removalPolicy: RemovalPolicy.DESTROY,
    });

    const efs = new EfsConstruct( this, 'EFS', { vpc, });
    const db = new DbConstruct( this, 'Aurora', { vpc, });
    const batch = new BatchConstruct( this, 'Batch', { vpc, efs, db });
    const stateMachine = new CloudStateMachine( this, 'CloudCity', { vpc, efs, db, batch });
  }
}

module.exports = { CloudCityStack }
