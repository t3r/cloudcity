const { RemovalPolicy, Stack, } = require('aws-cdk-lib');
const s3 = require('aws-cdk-lib/aws-s3');


class CloudCityStack extends Stack {
  constructor(scope, id, props) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, 'VPC', {
      natGateways: 0,
      maxAzs: 3,
      enableDnsHostnames: true,
      enableDnsSupport: true,
      subnetConfiguration: [
      {
        name: 'public-cloudcity-1',
        subnetType: ec2.SubnetType.PUBLIC,
      },
      {
        name: "private-subnet",
        subnetType: ec2.SubnetType.ISOLATED,
        cidrMask: 24,
      },
      ],
    });

  }
}

module.exports = { CloudCityStack }
