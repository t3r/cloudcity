// FIXME: don't create 1x1s, better create a job for each btg tile
exports.handler = async (tile) => {
  console.log("calculating tiles for ", JSON.stringify(tile) );
  const reply = [];

  const y_step = 0.125;

  for (let y = +tile.south; y < +tile.north; y += y_step) {
    for (let x = +tile.west; x < +tile.east; x += 1) {
      reply.push({
        database: tile.database,
        west: x,
        east: x + 1,
        south: y,
        north: y + y_step
      })
    }
  }
  console.log("responding with ", JSON.stringify(reply) );
  return reply;
};

