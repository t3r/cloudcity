const { RemovalPolicy} = require('aws-cdk-lib');
const ec2 = require('aws-cdk-lib/aws-ec2');
const rds = require('aws-cdk-lib/aws-rds');
const { Construct } = require('constructs');

const SG = Symbol("SG");
const DB = Symbol("DB");
class DbConstruct extends Construct {
  constructor(scope, id, props) {
    super(scope, id, props);

    const securityGroup = new ec2.SecurityGroup(this, id + 'SecurityGroup', {
      vpc: props.vpc,
    });
    securityGroup.addIngressRule(
      ec2.Peer.anyIpv4(),
      ec2.Port.tcp(5432),
      'allow inbound traffic from anywhere to the db on port 5432'
    );

    this[DB] = new rds.DatabaseCluster(this, id + 'Cluster', {
      vpc: props.vpc,
      vpcSubnets: props.vpc.selectSubnets({
        subnetType: ec2.SubnetType.PUBLIC,
      }),
      securityGroups: [ securityGroup ],

      engine: rds.DatabaseClusterEngine.auroraPostgres({
        version: rds.AuroraPostgresEngineVersion.VER_15_2,
      }),
      serverlessV2MinCapacity: 0.5,
      serverlessV2MaxCapacity: 2.0,
      clusterIdentifier: 'cloudcity',
      removalPolicy: RemovalPolicy.DESTROY,
      writer: rds.ClusterInstance.serverlessV2('writer'),
      defaultDatabaseName: 'osm',
      port: 5432,
    });
  }

  get cluster() { return this[DB]; }
}


module.exports = { DbConstruct }
