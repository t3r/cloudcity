const { Duration } = require('aws-cdk-lib');
const sqs = require('aws-cdk-lib/aws-sqs');
const { Construct } = require('constructs');

const Q = Symbol("Q");
class QueueConstruct extends Construct {
  constructor(scope, id, props) {
    super(scope, id, props);

    this[Q] = new sqs.Queue( this, id, {
      fifo: true,
      maxMessageSizeBytes: 1024,
      contentBasedDeduplication: false,
      visibilityTimeout: Duration.hours(12),
      retentionPeriod: Duration.days(7),
    });
  }

  get queue() { return this[Q] }
}

module.exports = { QueueConstruct }
