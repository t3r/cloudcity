const sfn = require('aws-cdk-lib/aws-stepfunctions');
const tasks = require('aws-cdk-lib/aws-stepfunctions-tasks');
const iam = require('aws-cdk-lib/aws-iam');
const lambda = require('aws-cdk-lib/aws-lambda');
const path = require('path');
const fs = require('fs');
const { Construct } = require('constructs');


class CloudStateMachine extends Construct {
  constructor(scope, id, props) {
    super(scope, id, props);

    const role =  new iam.Role(this, id + 'StateMachineRole', {
      assumedBy: new iam.ServicePrincipal('states.amazonaws.com'),
      description: 'StateMachine Role for cloud-city',
    });

    role.attachInlinePolicy(new iam.Policy(this, id + 'StepFunction', {
/*
  https://ataiva.com/how-to-fix-is-not-authorized-to-create-managed-rule-in-aws-step-functions/
  add CloudWatchEventsFullAccess
*/
      statements: [
        new iam.PolicyStatement({
          actions: [  "batch:SubmitJob", "batch:DescribeJobs", "batch:TerminateJob", "states:StartExecution" ],
          effect: iam.Effect.ALLOW,
          resources: [ "*" ] 
        }),
        new iam.PolicyStatement({
          actions: [ "xray:PutTraceSegments", "xray:PutTelemetryRecords", "xray:GetSamplingRules", "xray:GetSamplingTargets" ],
          effect: iam.Effect.ALLOW,
          resources: [ "*" ] 
        })
      ]
    }));

    const buildTile = new tasks.BatchSubmitJob( this, 'Build1x1', {
      jobName: 'Build1x1',
      jobDefinitionArn: props.batch.getTaskDefinition('Ec2Builder').jobDefinitionArn,
      jobQueueArn: props.batch.ec2OnDemandQueue.jobQueueArn,
      payload: sfn.TaskInput.fromObject({
        "json.$": "States.JsonToString($)"
      }),
    })

    const create1x1Lambda = new lambda.Function(this, 'Function', {
      runtime: lambda.Runtime.NODEJS_20_X,
      architecture: lambda.Architecture.ARM_64,
      handler: 'index.handler',
      code: lambda.Code.fromInline(fs.readFileSync(path.join(__dirname, 'create1x1.js'),{ encoding: 'utf8', flag: 'r' })),
    });

    const create1x1Tiles = new tasks.LambdaInvoke( this, 'Create1x1', {
      lambdaFunction: create1x1Lambda,
      payLoad: sfn.TaskInput.fromJsonPathAt('$.Body')
    });

    const forEvery1x1 = new sfn.Map(this, 'ForEvery1x1', {
      maxConcurrency: 100,
      itemsPath: sfn.JsonPath.stringAt('$.Payload'),
    });
    forEvery1x1.itemProcessor( 
      buildTile
        .next( new sfn.Succeed(this,'Tile Created' ) ), 
      {
          mode: sfn.ProcessorMode.DISTRIBUTED,
          executionType: sfn.ProcessorType.STANDARD
      }
    )

    const planetExtract = new tasks.BatchSubmitJob( this, 'PlanetExtract', {
      jobName: 'PlanetExtract',
      jobDefinitionArn: props.batch.getTaskDefinition('FgImportPlanet').jobDefinitionArn,
      jobQueueArn: props.batch.fargateQueue.jobQueueArn,
      payload: sfn.TaskInput.fromObject({
        "json.$": "States.JsonToString($)"
      }),
      resultPath: sfn.JsonPath.DISCARD,
    })

    const forEvery10x10 = new sfn.Map(this, 'ForEvery10x10', {
      itemsPath: sfn.JsonPath.stringAt('$.Tiles'),
      maxConcurrency: 10,
    });

    forEvery10x10.itemProcessor( 
      planetExtract
        .next( create1x1Tiles )
        .next( forEvery1x1 )
        .next( new sfn.Pass( this, 'DropDB' ) )
        .next( new sfn.Succeed(this,'Job complete' ) )
    )

    const choice = new sfn.Choice(this, 'Are there tiles to process?');
    choice
      .when( sfn.Condition.isPresent('$.Tiles'), forEvery10x10 )
      .otherwise( new sfn.Succeed(this,'Finish') );

    new sfn.StateMachine( this, 'TileBuilder', {
      role,
      definitionBody: sfn.DefinitionBody.fromChainable( choice ),
    });

  }
}

module.exports = { CloudStateMachine }
