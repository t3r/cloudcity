import { handler } from './tiles.js';

async function test() {
  const response = await handler({
    routeKey: 'GET /items/{id}',
    pathParameters: {
      id: 244234,
    },
  });
  console.log(response);
}

test();
