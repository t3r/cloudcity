#!/usr/bin/env node

require('dotenv' ).config();


const cdk = require('aws-cdk-lib');
const { CloudCityStack } = require('../lib/cloud-city-stack');

const app = new cdk.App();

new CloudCityStack(app, 'CloudCity', {
  env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },
  description: 'Process OSM2City for the entire planet and build Cloud City',
});

cdk.Tags.of(app).add('app', 'cloud-city');
