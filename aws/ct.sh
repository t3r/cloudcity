#  --task-definition=OsmworldStackOsm2CityTask93129155 \

aws ecs run-task \
  --launch-type=FARGATE \
  --cluster=OsmworldStack-FlightGearOsmWorlddev381A089C-fXfFgdEefbmr \
  --task-definition=OsmworldStackOsm2CityTask93129155\
  --count=1 \
  '--network-configuration={"awsvpcConfiguration":{"subnets":["subnet-7a2b4e11","subnet-7355880e","subnet-34a72379"],"securityGroups":["sg-cff7f6a5"],"assignPublicIp":"ENABLED"}}' \
  '--overrides={"containerOverrides":[{"name":"osm2city","command":["ls", "-l", "/workspace"]}]}'
